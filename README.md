 # Trabalho Prático - Comparação de sequências de DNA sobre a espécie Gorilla

library("seqinr")
gorilla <- read.fasta( file="C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\gorilla.fasta")

#exercicio 3.1.1

#tamanho da sequência
tam <-length(gorilla[[1]])
tam

# exercicio 3.1.2

#contagem de cada nuclótido
table(gorilla)

#exercicio 3.1.3

gorillaseq<-gorilla[[1]]

#frequência de cada nucleótido
count(gorillaseq,1,freq=T)

#número de ocorrências de cada nucleótido
count(gorillaseq,1)

#exercicio 3.1.4

#frequência da palavra DNA de comprimento2
pal2<-count(gorillaseq,2,freq=T)
pie((pal2)) # gráfico circular

#frequência da palavra DNA de comprimento3
pal3<-count(gorillaseq,3,freq=T)
pie((pal3)) # gráfico circular


# exercicio 3.1.5

# número de ocorências do codão de start e dos codões de stop

#codão de START
count(gorillaseq,3)["atg"] 

#codões de STOP
count(gorillaseq,3)["taa"]
count(gorillaseq,3)["tag"]
count(gorillaseq,3)["tga"]

#exercicio 3.1.6 - ORF
gorillaseq <- gorilla[[1]]
gorillastring <- c2s(gorillaseq)


gorilla_codao <- sapply(seq(1, nchar(gorillastring), 3), function(x) substring(gorillastring, x, x + 2))

tam <- length(gorilla_codao)
atg <- numeric()#inicializar vetor nulo
cont <- 0
for (i in 1:tam) {
  if (gorilla_codao[[i]] == "atg") {
    cont <- cont + 1 #conta os atg's
    atg[cont] <- i #para cada atg, guarda no vetor
  }
}
atg#mostra as posiçoes onde estao os atg
pos_start <- atg #guarda num vetor posição inicial"


pos_stop <-numeric()#vetor que vai ter as posiçoes dos codoes de finalização
cont <- 0
for (i in 1:tam) {
  if (gorilla_codao[[i]] == "taa" || gorilla_codao[[i]] == "tga" || gorilla_codao[[i]] == "tag") {
    cont <- cont + 1 #conta os codoes de finalização
    pos_stop[cont] <- i #para cada codao, guarda no vetor
  }
}
pos_start
pos_stop

i <- 1
cnt <- 0
while ((pos_stop[i] <= pos_start[1]) == TRUE) {
  #como o primeiro atg aparece na posiçao 30, este ciclo serve para limpar o que esta para tras
  repeat {
    #repete a instrução até encontrar a condição que está dentro do ciclo
    i <- i + 1
    cnt <- i
    break #quando encontrar o 1º numero maior que 30, ele guarda-o
  }
}

tamps <- length(pos_stop)
pos_stop <- pos_stop[cnt:tamps]#escreve um novo vetor que começa no indice 4 onde a posição final é maior que a primeira posição inicial
pos_stop

for (i in 1:length(pos_start))
  #aqui a ideia era:
{
  if ((pos_stop[i] < pos_start[i]) == TRUE)
    #ao comparar cada numero dos dois vetores, quando aparece-se um codao de finalização sem um codao de iniciação antes, ele retirava-o
  {
    pos_stop <- pos_stop[-i]
  }
}
pos_stop

#listar o primeiro orf da sequencia
c2s(gorilla_codao[pos_start[1]:pos_stop[1]])


#exercicio 3.1.7.
  # exerciico 3.1.7.1

# determinação do conteúdo CG e respetivo gráfico
slidingwindowplot <- function(windowsize, inputseq)
{
  starts <- seq(1, length(inputseq)-windowsize)
  n <- length(starts) #encontra o comprimento do vector"starts"
  chunkGCs <- numeric(n) # faz um vetor do mesmo comprimento que o vetor starts mas só contendo zeros
  for (i in 1:n) {
    chunk <- inputseq[starts[i]:(starts[i]+windowsize-1)]
    chunkGC <- GC(chunk)
    chunkGCs[i] <- chunkGC
  }
  plot(starts,chunkGCs,type="l",xlab="Posição inicial do nucleótideo",ylab="Conteúdo GC")
}
slidingwindowplot(1000,gorillaseq)

  # exercicio 3.1.7.2

# determinação do conteúdo AT e respetivo gráfico
slidingwindowplotAT <- function(windowsize, inputseq)
{
  starts <- seq(1, length(inputseq)-windowsize)
  n <- length(starts) # encontra o comprimento do vector"starts"
  chunkATs <- numeric(n) # faz um vetor do mesmo comprimento que o vetor starts mas só contendo zeros
  for (i in 1:n) {
    chunk <- inputseq[starts[i]:(starts[i]+windowsize-1)]
    chunkAT <- count(chunk,1,freq=T)["a"]+count(chunk,1,freq=T)["t"]
    chunkATs[i] <- chunkAT
  }
  plot(starts,chunkATs,type="l",xlab="Posição inicial do nucleótideo",ylab="Conteúdo AT")
}


slidingwindowplotAT(1000,gorillaseq)

  # exercicio 3.1.7.3

#frequências de cada nucléotido e respetivo gráfico
slidingwindowplotNucleotidos <- function(windowsize, inputseq)
{
  starts <- seq(1, length(inputseq)-windowsize)
  n <- length(starts) # encontra o comprimento do vector"starts"
  chunkAs <- numeric(n) # faz um vetor do mesmo comprimento que o vetor starts mas só contendo zeros
  chunkTs <- numeric(n)
  chunkCs <- numeric(n)
  chunkGs <- numeric(n)
  for (i in 1:n) {
    chunk <- inputseq[starts[i]:(starts[i]+windowsize-1)]
    chunkA <- count(chunk,1,freq=T)["a"]
    chunkT <- count(chunk,1,freq=T)["t"]
    chunkC <- count(chunk,1,freq=T)["c"]
    chunkG <- count(chunk,1,freq=T)["g"]
    chunkAs[i] <- chunkA
    chunkTs[i] <- chunkT
    chunkCs[i] <- chunkC
    chunkGs[i] <- chunkG
    
  }
  lim2 <- max(chunkAs,chunkTs,chunkGs,chunkCs)
  lim1 <- min(chunkAs,chunkTs,chunkGs,chunkCs)
  plot(starts,chunkGs,ylim=range(c(lim1,lim2)),type="l",xlab="",ylab="", col = "red")
  par(new=TRUE)
  plot(starts,chunkAs,ylim=range(c(lim1,lim2)),type="l",xlab="",ylab="", col = "blue")
  par(new=TRUE)
  plot(starts,chunkTs,ylim=range(c(lim1,lim2)),type="l",xlab="",ylab="", col = "black")
  par(new=TRUE)
  plot(starts,chunkCs,ylim=range(c(lim1,lim2)),type="l",xlab="",ylab="", col = "green")
  legend("topleft", legend = c("G", "A","T","C"), text.col = c("red","blue","black","green"),box.lty=0,horiz=T)
}

slidingwindowplotNucleotidos(1000,gorillaseq)